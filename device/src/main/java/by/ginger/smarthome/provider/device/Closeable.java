/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package by.ginger.smarthome.provider.device;

/**
 *
 * @author mirash
 */
public interface Closeable {

    public void close();
}
