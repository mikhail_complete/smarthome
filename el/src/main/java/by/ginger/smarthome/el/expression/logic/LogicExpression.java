/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package by.ginger.smarthome.el.expression.logic;

import by.ginger.smarthome.el.context.EvaluationContext;
import by.ginger.smarthome.el.expression.AbstractExpression;

/**
 *
 * @author mirash
 */
public abstract class LogicExpression extends AbstractExpression {

    public LogicExpression(EvaluationContext context) {
        super(context);
    }
}
