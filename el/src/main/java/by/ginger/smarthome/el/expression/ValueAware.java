/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package by.ginger.smarthome.el.expression;

/**
 *
 * @author mirash
 */
public interface ValueAware<V> {
    
    public void setValue(V value);
    
}
