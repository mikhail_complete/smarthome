/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package by.ginger.smarthome.el.expression;

/**
 *
 * @author mirash
 */
public interface ExpressionAware {

    public void setExprOne(Expression exprOne);

    public void setExprAnother(Expression exprAnother);
}
