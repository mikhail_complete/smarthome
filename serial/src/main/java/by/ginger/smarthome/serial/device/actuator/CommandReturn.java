/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package by.ginger.smarthome.serial.device.actuator;

/**
 *
 * @author mirash
 */
public enum CommandReturn {
    READING_STATE,
    OPEN,
    CLOSE
}
